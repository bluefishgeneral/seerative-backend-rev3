from flask import Flask
from flask import request
from flask import session
from flask_session import Session
import multiprocessing

import urllib
from pyngrok import ngrok

import os
import zipfile

import urllib

from fake_useragent import UserAgent
from http.cookiejar import CookieJar
import base64


# some_file.py
import sys
################################### GPT-2 Imports
'''
Initialize tensorflow, initialize OpenAI's GPT-2, and initialize a tensorflow session to avoid bottlenecks.
 
'''
def gpt_init():
    global sess
    global gpt2
    os.environ["CUDA_VISIBLE_DEVICES"]="-1"  # specify which GPU(s) is to be used. The value "-1" forces the program to run on CPU instead.

    import gpt_2_simple as gpt2
    import time
    from sys import modules
    from os.path import basename, splitext


    # start tensorflow session to load later
    sess = gpt2.start_tf_sess()
    # load GPT-2 so that every user does not have to wait for initialization
    gpt2.load_gpt2(sess, run_name="run1")

    return [sess, gpt2]    
gpt_init()
################################### Tensorflow Graph Saving Imports
'''
Initialize a tensorflow graph in order to keep track  tensorflow model
between user sessions. This will enable each and every user to point to one tensorflow instance.

Allowing every user to share one tensorflow instance is what allows this program to run on a single standard virtual CPU (Azure).
 
'''
def graph_init():
    global graph
    import tensorflow as tf
    
    graph = tf.get_default_graph() 
    return graph
graph_init()
###################################

app = Flask(__name__)

'''
This method just enables us to make a call to the server URL and get back a response from the GPT-2 AI.

params:
- txt_in: the beginning text. GPT-2 will use this to predict the rest.
- ver: the version of GPT-2 that you want to use. Currently only ~100M sized models of GPT-2 are available for this (since it runs the best on 1 vCPU.
- len: the preferred length of the response text from GPT-2. 
'''
@app.route('/', methods = ['GET'])
def get_text():
    argsN = request.args
    try:
        print("Task running")
        txt = str(argsN.get('txt_in').strip())
        ver = str(argsN.get('ver').strip())
        txt_len = str(argsN.get('len').strip())
        print("Task complete")
        with graph.as_default():
            if '01' in ver:
                return gpt2.generate(sess,return_as_list=True, run_name="run1", length=int(txt_len), prefix=txt)[0]

            if '02' in ver:
                return gpt2.generate(sess,return_as_list=True, run_name="run1", length=int(txt_len), prefix=txt)[0]
        
    except:
        return "Not available at the moment. Try comming back tomorrow."

if __name__ == '__main__':
    # This is to prevent multiple users accessing the server at the same time. Because if that happens, this program will crash on 1 vCPU.
    multiprocessing.freeze_support()

    # run server on a specified port.
    PORT_NUM = 8001
    
    app.run(host='0.0.0.0', port=PORT_NUM)


    


